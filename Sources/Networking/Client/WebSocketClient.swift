//
//  Created on 30.09.2023.
//  Copyright © 2023 Коба Самхарадзе. All rights reserved.
//

import Foundation

public enum WebSocketError: Int, Error {
    case connectionResetByPeer = 54
    case notConnected = 57
    case operationTimedOut = 60
}

public protocol WebSocketEventDelegate: AnyObject {
    func connect()

    func disconnect(
        closeCode: URLSessionWebSocketTask.CloseCode,
        reason: Data?
    )

    func receiveMessage(
        _ message: URLSessionWebSocketTask.Message,
        client: WebSocketClient
    )

    func receiveError(
        _ error: Error,
        client: WebSocketClient
    )
}

open class WebSocketClient: NetworkClient,
                            URLSessionWebSocketDelegate {

    private var hasActiveConnection: Bool = false

    public let request: URLRequest

    public var pingIntervalSeconds: TimeInterval = 5

    public private(set) lazy var webSocketTask: URLSessionWebSocketTask = self.session.webSocketTask(with: self.request)

    public weak var eventDelegate: WebSocketEventDelegate?

    public var withReconnect: Bool = true

    public init(
        configuration: URLSessionConfiguration,
        delegateQueue queue: OperationQueue?,
        request: URLRequest,
        logger: Logger?
    ) {
        self.request = request
        super.init(
            configuration: configuration,
            delegateQueue: queue,
            logger: logger
        )
    }

    open func connect() {
        guard self.hasActiveConnection == false else { return }

        self.hasActiveConnection = true
        self.webSocketTask.resume()
    }

    open func disconnect() {
        guard self.hasActiveConnection else { return }

        self.hasActiveConnection = false
        self.webSocketTask.cancel(with: .goingAway, reason: nil)
    }

    open func sendMessage(_ message: URLSessionWebSocketTask.Message) async throws {
        guard self.hasActiveConnection else { throw WebSocketError.notConnected }

        try await self.webSocketTask.send(message)
    }

    private func receive() {
        Task { [weak self] in
            guard let self else { return }

            do {
                let message = try await self.webSocketTask.receive()
                self.eventDelegate?.receiveMessage(message, client: self)
                self.receive()
            } catch let error {
                let code = (error as NSError).code

                if let wsError = WebSocketError(rawValue: code) {
                    self.eventDelegate?.receiveError(wsError, client: self)

                    switch wsError {
                    case .connectionResetByPeer:
                        self.disconnect()

                    case .notConnected, .operationTimedOut:
                        if self.hasActiveConnection {
                            self.reconnectIfNeeded()
                        } else {
                            self.disconnect()
                        }
                    }
                } else {
                    self.eventDelegate?.receiveError(error, client: self)
                    self.receive()
                }
            }
        }
    }

    private func ping() {
        self.webSocketTask.sendPing { [weak self] error in
            guard let self else { return }

            if let error {
                self.eventDelegate?.receiveError(error, client: self)
            } else {
                Task { [weak self] in
                    guard let self else { return }

                    try await Task.sleep(nanoseconds: NSEC_PER_SEC * UInt64(self.pingIntervalSeconds))
                    self.ping()
                }
            }
        }
    }

    private func reconnectIfNeeded() {
        if self.withReconnect {
            self.connect()
        } else {
            self.disconnect()
        }
    }

    // MARK: - URLSessionWebSocketDelegate

    open func urlSession(
        _ session: URLSession,
        task: URLSessionTask,
        didCompleteWithError error: Error?
    ) {
        if let error {
            self.eventDelegate?.receiveError(error, client: self)
        }
    }

    open func urlSession(
        _ session: URLSession,
        webSocketTask: URLSessionWebSocketTask,
        didOpenWithProtocol protocol: String?
    ) {
        self.eventDelegate?.connect()
        self.ping()
        self.receive()
    }

    open func urlSession(
        _ session: URLSession,
        webSocketTask: URLSessionWebSocketTask,
        didCloseWith closeCode: URLSessionWebSocketTask.CloseCode,
        reason: Data?
    ) {
        self.eventDelegate?.disconnect(closeCode: closeCode, reason: reason)
    }
}
