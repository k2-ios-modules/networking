//
//  Created on 30.09.2023.
//  Copyright © 2023 Коба Самхарадзе. All rights reserved.
//

import Foundation

public typealias NetworkLog = [String: RequestLogDescriptionProtocol]

open class NetworkClient: NSObject,
                          URLSessionDelegate {

    public typealias Logger = ClientNetworkLoggerProtocol

    public private(set) var session: URLSession = .shared

    public var log: NetworkLog = [:]

    public let logger: Logger?

    public init(
        session: URLSession,
        logger: Logger?
    ) {
        self.session = session
        self.logger = logger
        super.init()
    }

    public init(
        configuration: URLSessionConfiguration,
        delegateQueue queue: OperationQueue?,
        logger: Logger?
    ) {
        self.logger = logger
        super.init()
        self.session = .init(
            configuration: configuration,
            delegate: self,
            delegateQueue: queue
        )
    }

    public final func cancelTasks() {
        self.session.getAllTasks(completionHandler: { tasks in
            tasks.forEach({ $0.cancel() })
        })
    }

    public final func showLog(by key: String) {
        guard let log = self.log[key] else { return }
        self.logger?.logCommon(log)
    }
}

