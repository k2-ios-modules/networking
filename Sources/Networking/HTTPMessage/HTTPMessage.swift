//
//  Created on 28.12.2023.
//  Copyright © 2023 Коба Самхарадзе. All rights reserved.
//  

import Foundation

public final class HTTPMessage {
    public let method: String

    public private(set) var query: [String: Any]?
    public var queryEncoding: QueryEncoding = .default

    public let bodyEncoding: BodyEncoding?

    init(
        method: String,
        bodyEncoding: BodyEncoding?
    ) {
        self.method = method
        self.bodyEncoding = bodyEncoding
    }

    public func withQuery(_ query: [String: Any]?) -> Self {
        self.query = query
        return self
    }

    public static func `get`() -> Self {
        .init(method: "GET", bodyEncoding: nil)
    }

    public static func `get`(_ query: [String: Any]?) -> Self {
        .init(method: "GET", bodyEncoding: nil).withQuery(query)
    }

    public static func post(_ bodyEncoding: BodyEncoding?) -> Self {
        .init(method: "POST", bodyEncoding: bodyEncoding)
    }

    public static func put(_ bodyEncoding: BodyEncoding?) -> Self {
        .init(method: "PUT", bodyEncoding: bodyEncoding)
    }

    public static func patch(_ bodyEncoding: BodyEncoding?) -> Self {
        .init(method: "PATCH", bodyEncoding: bodyEncoding)
    }

    public static func delete() -> Self {
        .init(method: "DELETE", bodyEncoding: nil)
    }
}
