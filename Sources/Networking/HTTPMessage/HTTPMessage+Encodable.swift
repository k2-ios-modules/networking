//
//  Created on 28.12.2023.
//  Copyright © 2023 Коба Самхарадзе. All rights reserved.
//  

import Foundation

public extension HTTPMessage {
    func withQuery(_ query: Encodable?) -> Self {
        self.withQuery(try? query?.asDictionary())
    }

    static func get(_ query: Encodable?) -> Self {
        .get(try? query?.asDictionary())
    }
}
