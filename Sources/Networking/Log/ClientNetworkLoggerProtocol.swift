//
//  Created on 28.12.2023.
//  Copyright © 2023 Коба Самхарадзе. All rights reserved.
//  

import Foundation

public protocol ClientNetworkLoggerProtocol: AnyObject {
    func log(_ values: Any..., file: String, line: Int, function: String)
}

public extension ClientNetworkLoggerProtocol {
    func logCommon(_ values: Any..., file: String = #file, line: Int = #line, function: String = #function) {
        log(values, file: file, line: line, function: function)
    }
}
