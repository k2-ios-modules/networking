//
//  Created on 28.12.2023.
//  Copyright © 2023 Коба Самхарадзе. All rights reserved.
//  

import Foundation

public enum RequestLogResponseType {
    case data(Data)
    case fileUrl(URL?)
    case none
}

public protocol RequestLogDescriptionProtocol: CustomDebugStringConvertible {
    var id: String { get }
    var date: Date { get }

    var httpStatusCode: Int? { get set }
    var responseType: RequestLogResponseType { get set }
    var error: Error? { get set }

    var dateDescription: String { get }
    var headerDescription: String? { get }
    var bodyDescription: String? { get }
    var responseDescription: String? { get }
    var errorDescription: String? { get }
}

public extension RequestLogDescriptionProtocol {
    var debugDescription: String {
        [
            "📡📡📡",
            "\(self.dateDescription)",
            self.headerDescription,
            self.bodyDescription,
            self.responseDescription,
            self.errorDescription,
            "📡📡📡"
        ]
            .compactMap({ $0 })
            .joined(separator: "\n")
    }
}
