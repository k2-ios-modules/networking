//
//  Created on 30.09.2023.
//  Copyright © 2023 Коба Самхарадзе. All rights reserved.
//

import Foundation

public typealias HTTPHeaders = [HTTPHeader: String]

public enum HTTPHeader: Hashable {
    public var rawValue: String {
        switch self {
        case .contentDisposition: return "Content-Disposition"
        case .contentType: return "Content-Type"
        case .contentLength: return "Content-Length"
        case .authorization: return "Authorization"
        case .acceptLanguage: return "Accept-Language"
        case .custom(let value): return value
        }
    }

    case contentDisposition
    case contentType
    case contentLength
    case authorization
    case acceptLanguage
    case custom(String)
}
