//
//  Created on 28.12.2023.
//  Copyright © 2023 Коба Самхарадзе. All rights reserved.
//  

import Foundation

enum MimeType: String, CaseIterable {
    case avi, asx, asf, ai, atom
    case bin, bmp
    case css, crt, cco
    case docx, doc, dll, der, dmg, deb
    case ear, eps, exe, eot
    case flv
    case gif
    case heic, htm, hqx, htc, html, heif
    case ico, iso, img
    case js, jar, json, jng, jnlp, jad, jardiff, jpeg, jpg
    case kar, key, kml, kmz
    case mpeg, mpg, msp, mp4, mng, mov, mml, midi, m3u8, m4a, mid, msi, mp3, msm, m4v
    case numbers
    case ogg
    case pptx, pages, pm, png, prc, pem, ps, ppt, pl, pdb, pdf
    case rtf, rss, run, rar, ra, rpm
    case swf, sit, svgz, sea, svg, shtml
    case tcl, txt, tk, tif, ts, tiff
    case wmv, woff, war, wml, webm, webp, wmlc, wbmp
    case xspf, xhtml, xpi, xlsx, xml, xls
    case zip
    case _3gp = "3pg",
         _3gpp = "3gpp",
         _7z = "7z"

    init?(contentType: String) {
        guard let first = Self.allCases.first(where: { $0.contentType == contentType }) else { return nil }
        self = first
    }

    init?(url: URL) {
        guard let type = Self(rawValue: url.pathExtension.lowercased())
        else { return nil }
        self = type
    }

    init?(fileName: String) {
        guard let type = Self(rawValue: String(fileName.split(separator: ".").last ?? "").lowercased())
        else { return nil }
        self = type
    }

    var isImage: Bool {
        switch self {
        case .gif, .jpg, .jpeg, .bmp, .webp, .png, .tiff, .tif:
            return true
        default:
            return false
        }
    }

    var isFile: Bool {
        switch self {
        case .txt, .rtf, .pdf, .doc, .docx, .xls, .xlsx, .ppt, .pptx, .mp4, .key, .pages, .numbers:
            return true
        default:
            return false
        }
    }

    var contentType: String {
        switch self {
        case .wmlc:
            return "application/vnd.wap.wmlc"
        case .wmv:
            return "video/x-ms-wmv"
        case .asx, .asf:
            return "video/x-ms-asf"
        case .hqx:
            return "application/mac-binhex40"
        case .gif:
            return "image/gif"
        case .pdb, .prc:
            return "application/x-pilot"
        case .jpeg, .jpg:
            return "image/jpeg"
        case .heic:
            return "image/heic"
        case .png:
            return "image/png"
        case .jng:
            return "image/x-jng"
        case .sea:
            return "application/x-sea"
        case .mp4:
            return "video/mp4"
        case .wml:
            return "text/vnd.wap.wml"
        case ._7z:
            return "application/x-7z-compressed"
        case .rss:
            return "application/rss+xml"
        case .atom:
            return "application/atom+xml"
        case .kmz:
            return "application/vnd.google-earth.kmz"
        case .m4a:
            return "audio/x-m4a"
        case .css:
            return "text/css"
        case .mml:
            return "text/mathml"
        case .ts:
            return "video/mp2t"
        case .js:
            return "application/javascript"
        case .ppt:
            return "application/vnd.ms-powerpoint"
        case .kml:
            return "application/vnd.google-earth.kml+xml"
        case .m3u8:
            return "application/vnd.apple.mpegurl"
        case .xhtml:
            return "application/xhtml+xml"
        case .cco:
            return "application/x-cocoa"
        case .xpi:
            return "application/x-xpinstall"
        case .swf:
            return "application/x-shockwave-flash"
        case .wbmp:
            return "image/vnd.wap.wbmp"
        case .flv:
            return "video/x-flv"
        case .numbers:
            return "application/x-iwork-numbers-sffnumbers"
        case .eps, .ai, .ps:
            return "application/postscript"
        case .jad:
            return "text/vnd.sun.j2me.app-descriptor"
        case .doc:
            return "application/msword"
        case .mov:
            return "video/quicktime"
        case .rar:
            return "application/x-rar-compressed"
        case .docx:
            return "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
        case ._3gp, ._3gpp:
            return "video/3gpp"
        case .pptx:
            return "application/vnd.openxmlformats-officedocument.presentationml.presentation"
        case .rtf:
            return "application/rtf"
        case .pages:
            return "application/x-iwork-pages-sffpages"
        case .shtml, .html, .htm:
            return "text/html"
        case .run:
            return "application/x-makeself"
        case .jardiff:
            return "application/x-java-archive-diff"
        case .pdf:
            return "application/pdf"
        case .woff:
            return "application/font-woff"
        case .tk, .tcl:
            return "application/x-tcl"
        case .pem, .crt, .der:
            return "application/x-x509-ca-cert"
        case .pl, .pm:
            return "application/x-perl"
        case .eot:
            return "application/vnd.ms-fontobject"
        case .ico:
            return "image/x-icon"
        case .htc:
            return "text/x-component"
        case .jnlp:
            return "application/x-java-jnlp-file"
        case .key:
            return "application/x-iwork-keynote-sffkey"
        case .webm:
            return "video/webm"
        case .svg, .svgz:
            return "image/svg+xml"
        case .zip:
            return "application/zip"
        case .ogg:
            return "audio/ogg"
        case .m4v:
            return "video/x-m4v"
        case .xlsx:
            return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        case .jar, .war, .ear:
            return "application/java-archive"
        case .msi, .iso, .msm, .bin, .img, .msp, .exe, .dll, .deb, .dmg:
            return "application/octet-stream"
        case .mng:
            return "video/x-mng"
        case .tiff, .tif:
            return "image/tiff"
        case .mp3:
            return "audio/mpeg"
        case .kar, .mid, .midi:
            return "audio/midi"
        case .bmp:
            return "image/x-ms-bmp"
        case .xls:
            return "application/vnd.ms-excel"
        case .sit:
            return "application/x-stuffit"
        case .xspf:
            return "application/xspf+xml"
        case .rpm:
            return "application/x-redhat-package-manager"
        case .ra:
            return "audio/x-realaudio"
        case .mpg, .mpeg:
            return "video/mpeg"
        case .json:
            return "application/json"
        case .xml:
            return "text/xml"
        case .heif:
            return "image/heif"
        case .webp:
            return "image/webp"
        case .avi:
            return "video/x-msvideo"
        case .txt:
            return "text/plain"
        }
    }
}
