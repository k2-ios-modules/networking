//
//  Created on 28.12.2023.
//  Copyright © 2023 Коба Самхарадзе. All rights reserved.
//  

import Foundation

public enum MultipartFile: Encodable {
    case url(URL)
    case data(Data, name: String)

    var data: Data? {
        switch self {
        case let .url(url):
            try? Data(contentsOf: url)

        case let .data(data, _):
            data
        }
    }

    var name: String {
        switch self {
        case let .url(url):
            url.lastPathComponent

        case let .data(_, name):
            name
        }
    }

    var mimeType: MimeType? {
        .init(fileName: self.name)
    }
}
