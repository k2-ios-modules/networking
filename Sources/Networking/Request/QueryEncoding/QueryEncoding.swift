//
//  Created on 30.09.2023.
//  Copyright © 2023 Коба Самхарадзе. All rights reserved.
//

import Foundation

public struct QueryEncoding {
    let bool: BoolEncoding
    let array: ArrayEncoding
}

public extension QueryEncoding {
    static var `default`: Self {
        .init(bool: .numeric, array: .sequence(.noBrackets))
    }
}
