//
//  Created on 30.09.2023.
//  Copyright © 2023 Коба Самхарадзе. All rights reserved.
//

import Foundation

public enum BoolEncoding {
    /// Encode `true` as `1` and `false` as `0`.
    case numeric
    /// Encode `true` and `false` as string literals.
    case literal
    case custom((Bool) -> String)

    func makeQueryItem(key: String, value: Bool) -> URLQueryItem {
        return URLQueryItem(
            name: key,
            value: self.encode(value: value)
        )
    }

    func encode(value: Bool) -> String {
        switch self {
        case .numeric: return value ? "1" : "0"
        case .literal: return value ? "true" : "false"
        case let .custom(encoding): return encoding(value)
        }
    }
}
