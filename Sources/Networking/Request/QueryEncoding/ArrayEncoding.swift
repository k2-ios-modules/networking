//
//  Created on 30.09.2023.
//  Copyright © 2023 Коба Самхарадзе. All rights reserved.
//

import Foundation

public enum ArrayEncoding {
    case single(SingleSeparator)
    case sequence(SequenceKeyEncoding)

    func makeQueryItems(key: String, value: [Any]) -> [URLQueryItem] {
        switch self {
        case let .single(separator):
            return [URLQueryItem(
                name: key,
                value: separator.encode(value: value)
            )]

        case let .sequence(keyEncoding):
            return value.enumerated().map({
                return URLQueryItem(
                    name: keyEncoding.encode(key: key, atIndex: $0.offset),
                    value: .init(describing: $0.element)
                )
            })
        }
    }
}

public extension ArrayEncoding {
    enum SingleSeparator {
        case comma
        case custom(String)

        func encode(value: [Any]) -> String {
            let stringItems = value.map({ String(describing: $0) })
            switch self {
            case .comma: return stringItems.joined(separator: ",")
            case let .custom(separator): return stringItems.joined(separator: separator)
            }
        }
    }
}

public extension ArrayEncoding {
    enum SequenceKeyEncoding {
        /// An empty set of square brackets is appended to the key for every value.
        case brackets
        /// No brackets are appended. The key is encoded as is.
        case noBrackets
        /// Brackets containing the item index are appended. This matches the jQuery and Node.js behavior.
        case indexInBrackets
        /// Provide a custom array key encoding with the given closure.
        case custom((_ key: String, _ index: Int) -> String)

        func encode(key: String, atIndex index: Int) -> String {
            switch self {
            case .brackets: return "\(key)[]"
            case .noBrackets: return key
            case .indexInBrackets: return "\(key)[\(index)]"
            case let .custom(encoding): return encoding(key, index)
            }
        }
    }
}
