//
//  Created on 30.09.2023.
//  Copyright © 2023 Коба Самхарадзе. All rights reserved.
//

import Foundation

public extension URLRequest {

    // MARK: - init

    init(
        url: URL,
        message: HTTPMessage
    ) {
        let totalURL: URL
        if let query = message.query,
           var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true) {
            urlComponents.appendQuery(
                query,
                queryEncoding: message.queryEncoding
            )
            totalURL = urlComponents.url ?? url
        } else {
            totalURL = url
        }

        self.init(url: totalURL)

        self.httpMethod = message.method
        if let bodyEncoding = message.bodyEncoding {
            switch bodyEncoding {
            case let .jsonObject(object, encoding):
                try? encoding.encode(&self, body: object)

            case let .queryObject(object, encoding):
                try? encoding.encode(&self, body: object)

            case let .string(string, encoding):
                try? encoding.encode(&self, body: string)

            case let .multipartObject(object, encoding):
                try? encoding.encode(&self, body: object)
            }
        }
    }

    // MARK: - methods for headers

    func value(for header: HTTPHeader) -> String? {
        self.value(forHTTPHeaderField: header.rawValue)
    }

    mutating func addHeaders(_ header: HTTPHeaders) {
        header.forEach({ self.addValue($0.value, forHTTPHeader: $0.key) })
    }

    mutating func addValue(
        _ value: String,
        forHTTPHeader header: HTTPHeader
    ) {
        self.addValue(value, forHTTPHeaderField: header.rawValue)
    }

    mutating func setHeaders(_ header: HTTPHeaders) {
        header.forEach({ self.setValue($0.value, forHTTPHeader: $0.key) })
    }

    mutating func setValue(
        _ value: String?,
        forHTTPHeader header: HTTPHeader
    ) {
        self.setValue(value, forHTTPHeaderField: header.rawValue)
    }

    mutating func removeValue(forHTTPHeaders headers: [HTTPHeader]) {
        headers.forEach({ self.removeValue(forHTTPHeader: $0)} )
    }

    mutating func removeValue(forHTTPHeader header: HTTPHeader) {
        self.allHTTPHeaderFields?.removeValue(forKey: header.rawValue)
    }
}

