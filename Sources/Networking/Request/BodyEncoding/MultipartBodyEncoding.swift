//
//  Created on 28.12.2023.
//  Copyright © 2023 Коба Самхарадзе. All rights reserved.
//  

import Foundation

public protocol MultipartBodyEncodingProtocol: BodyEncodingProtocol
where T == [String: Any] {

}

public typealias AnyMultipartBodyEncoding = (any MultipartBodyEncodingProtocol)

public struct MultipartBodyEncoding: MultipartBodyEncodingProtocol {
    private static let crlf = "\r\n"

    public init() { }

    public func encode(
        _ urlRequest: inout URLRequest,
        body: [String: Any]?
    ) throws {
        let boundary = UUID().uuidString
        urlRequest.setValue(
            "multipart/form-data; boundary=\(boundary)",
            forHTTPHeader: .contentType
        )

        guard let body else { return }

        var httpBody = Data()

        let start = "--\(boundary)\(Self.crlf)"
        let middle = "\(Self.crlf)--\(boundary)--\(Self.crlf)"
        let finish = "\(Self.crlf)--\(boundary)--\(Self.crlf)"

        body.enumerated().forEach({
            let name = $0.element.key
            let value = $0.element.value
            let index = $0.offset

            httpBody.append(index > 0 ? middle : start)
            self.append(name: name, value: value, to: &httpBody)
            httpBody.append(finish)
        })

        urlRequest.httpBody = httpBody
    }

    private func append(
        name: String,
        value: Any,
        to data: inout Data
    ) {
        let valueData: Data?
        switch value {
        case let file as MultipartFile:
            data.append(self.makeHeaderData(
                name: name,
                file: file,
                mimeType: file.mimeType
            ))
            valueData = file.data

        case let array as [Any]:
            array.forEach({
                self.append(name: name, value: $0, to: &data)
            })
            valueData = nil

        case let number as NSNumber:
            data.append(self.makeHeaderData(name: name))
            valueData = number.stringValue.toData

        case let string as String:
            data.append(self.makeHeaderData(name: name))
            valueData = string.toData

        default:
            valueData = nil
        }

        guard let valueData else { return }
        data.append(valueData)
        data.append(Self.crlf)
    }

    private func makeHeaderData(
        name: String,
        file: MultipartFile? = nil,
        mimeType: MimeType? = nil
    ) -> String {
        var headers: [HTTPHeader: String] = [:]
        var disposition = "form-data; name=\"\(name)\""
        if let file {
            disposition += "; filename=\"\(file.name)\""
        }
        headers[.contentDisposition] = disposition

        if let mimeType {
            headers[.contentType] = mimeType.contentType
        }

        let string = headers.reduce("", { (result, next) in
            result + "\(next.key): \(next.value)" + Self.crlf
        }) + Self.crlf

        return string
    }
}

// MARK: - utils

private extension String {
    var toData: Data? {
        self.data(using: .utf8)
    }
}

private extension Data {
    mutating func append(_ string: String) {
        guard let data = string.toData else { return }
        self.append(data)
    }
}
