//
//  Created on 28.12.2023.
//  Copyright © 2023 Коба Самхарадзе. All rights reserved.
//  

import Foundation

public protocol JSONBodyEncodingProtocol: BodyEncodingProtocol
where T == [String: Any] {

}

public typealias AnyJSONBodyEncoding = (any JSONBodyEncodingProtocol)

public struct JSONBodyEncoding: JSONBodyEncodingProtocol {
    public init() { }

    public func encode(
        _ urlRequest: inout URLRequest,
        body: [String: Any]?
    ) throws {
        urlRequest.setValue(
            "application/json",
            forHTTPHeader: .contentType
        )

        guard let body else { return }

        urlRequest.httpBody = try JSONSerialization.data(
            withJSONObject: body,
            options: []
        )
    }
}
