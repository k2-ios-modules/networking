//
//  Created on 28.12.2023.
//  Copyright © 2023 Коба Самхарадзе. All rights reserved.
//  

import Foundation

public protocol QueryBodyEncodingProtocol: BodyEncodingProtocol
where T == [String: Any] {

}

public typealias AnyQueryBodyEncoding = (any QueryBodyEncodingProtocol)

public struct QueryBodyEncoding: QueryBodyEncodingProtocol {
    public init() { }

    public func encode(
        _ urlRequest: inout URLRequest,
        body: [String: Any]?
    ) throws {
        urlRequest.setValue(
            "application/x-www-form-urlencoded; charset=utf-8",
            forHTTPHeader: .contentType
        )

        guard let body else { return }

        let query = body
            .map({ "\($0.key)=\($0.value)" })
            .joined(separator: "&")
        urlRequest.httpBody = query.data(using: .utf8)
    }
}
