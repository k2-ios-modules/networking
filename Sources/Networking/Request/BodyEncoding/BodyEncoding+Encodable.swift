//
//  Created on 28.12.2023.
//  Copyright © 2023 Коба Самхарадзе. All rights reserved.
//  

import Foundation

public extension BodyEncoding {
    static func json(
        _ body: Encodable?,
        encoding: AnyJSONBodyEncoding = JSONBodyEncoding()
    ) -> Self {
        .jsonObject((try? body?.asDictionary()), encoding: encoding)
    }

    static func query(
        _ body: Encodable?,
        encoding: AnyQueryBodyEncoding = QueryBodyEncoding()
    ) -> Self {
        .queryObject((try? body?.asDictionary()), encoding: encoding)
    }

    static func multipart(
        _ body: Encodable?,
        encoding: AnyMultipartBodyEncoding = MultipartBodyEncoding()
    ) -> Self {
        .multipartObject((try? body?.asDictionary()), encoding: encoding)
    }
}
