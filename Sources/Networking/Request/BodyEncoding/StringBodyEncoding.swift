//
//  Created on 28.12.2023.
//  Copyright © 2023 Коба Самхарадзе. All rights reserved.
//  

import Foundation

public protocol StringBodyEncodingProtocol: BodyEncodingProtocol
where T == String {

}

public typealias AnyStringBodyEncoding = (any StringBodyEncodingProtocol)

public struct StringBodyEncoding: StringBodyEncodingProtocol {
    public init() { }

    public func encode(
        _ urlRequest: inout URLRequest,
        body: String?
    ) throws {
        urlRequest.setValue(
            "application/json",
            forHTTPHeader: .contentType
        )

        guard let body else { return }

        urlRequest.httpBody = body.data(using: .utf8)
    }
}
