//
//  Created on 28.12.2023.
//  Copyright © 2023 Коба Самхарадзе. All rights reserved.
//  

import Foundation

public enum BodyEncoding {
    case jsonObject(
        [String: Any]?,
        encoding: AnyJSONBodyEncoding = JSONBodyEncoding()
    )

    case queryObject(
        [String: Any]?,
        encoding: AnyQueryBodyEncoding = QueryBodyEncoding()
    )

    case string(
        String,
        encoding: AnyStringBodyEncoding = StringBodyEncoding()
    )

    case multipartObject(
        [String: Any]?,
        encoding: AnyMultipartBodyEncoding = MultipartBodyEncoding()
    )
}
