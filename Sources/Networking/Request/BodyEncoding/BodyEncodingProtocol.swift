//
//  Created on 28.12.2023.
//  Copyright © 2023 Коба Самхарадзе. All rights reserved.
//  

import Foundation

public protocol BodyEncodingProtocol {
    associatedtype T

    func encode(_ urlRequest: inout URLRequest, body: T?) throws
}
