//
//  Created on 28.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//

import Foundation

extension CheckedContinuation where T == (Data, URLResponse), E == Error {
    func resume(
        data: Data?,
        response: URLResponse?,
        error: Error?
    ) {
        if let error {
            self.resume(throwing: error)
        } else if let data, let response {
            self.resume(returning: (data, response))
        } else {
            self.resume(throwing: URLSessionError.unknownError)
        }
    }
}

extension CheckedContinuation where T == (URL, URLResponse), E == Error {
    func resume(
        url: URL?,
        response: URLResponse?,
        error: Error?
    ) {
        if let error {
            self.resume(throwing: error)
        } else if let url, let response {
            self.resume(returning: (url, response))
        } else {
            self.resume(throwing: URLSessionError.unknownError)
        }
    }
}

