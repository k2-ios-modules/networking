//
//  Created on 28.12.2023.
//  Copyright © 2023 Коба Самхарадзе. All rights reserved.
//  

import Foundation

extension String: LocalizedError {
    public var errorDescription: String? { self }
}
