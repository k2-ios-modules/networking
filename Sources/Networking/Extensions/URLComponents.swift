//
//  Created on 30.09.2023.
//  Copyright © 2023 Коба Самхарадзе. All rights reserved.
//

import Foundation

public extension URLComponents {
    mutating func appendQuery(
        _ query: [String: Any]?,
        queryEncoding encoding: QueryEncoding = .default
    ) {
        guard let query else { return }

        var queryItems = self.queryItems ?? []
        query.forEach({
            let key = $0.key
            let value = $0.value
            switch value {
            case let number as NSNumber:
                if number.isBool {
                    queryItems.append(
                        encoding.bool.makeQueryItem(key: key, value: number.boolValue)
                    )
                } else {
                    queryItems.append(
                        URLQueryItem(name: key, value: .init(describing: value))
                    )
                }

            case let array as [Any]:
                queryItems.append(
                    contentsOf: encoding.array.makeQueryItems(key: key, value: array)
                )

            default:
                queryItems.append(
                    URLQueryItem(name: key, value: .init(describing: value))
                )
            }
        })
        self.queryItems = queryItems.sorted(by: { $0.name < $1.name })
    }
}

fileprivate extension NSNumber {
    var isBool: Bool {
        String(cString: objCType) == "c"
    }
}
