//
//  Created on 28.12.2023.
//  Copyright © 2023 Коба Самхарадзе. All rights reserved.
//  

import Foundation

extension KeyedDecodingContainer {
    func decode<T: Decodable>(_ key: KeyedDecodingContainer<K>.Key) throws -> T {
        return try self.decode(T.self, forKey: key)
    }

    func decodeIfPresent<T: Decodable>(_ key: KeyedDecodingContainer<K>.Key) throws -> T? {
        return try self.decodeIfPresent(T.self, forKey: key)
    }
}
