//
//  Created on 30.09.2023.
//  Copyright © 2023 Коба Самхарадзе. All rights reserved.
//

import Foundation

public extension Encodable {
    func toData() throws -> Data {
        return try JSONEncoder().encode(self)
    }

    func asDictionary() throws -> [String: Any] {
        let data = try self.toData()
        guard let dictionary = try JSONSerialization.jsonObject(
            with: data,
            options: .fragmentsAllowed
        ) as? [String: Any] else {
            throw NSError()
        }
        return dictionary
    }
}
